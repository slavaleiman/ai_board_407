
import time
import serial
import sys
import struct
import math
import subprocess
import serial
import time
from time import sleep
import struct
import sys

if len(sys.argv) < 2:
    arg_port = "com7"
    # print("USAGE: %s " + sys.argv[0])
else:
    arg_port = sys.argv[1]

ser = serial.Serial(port=arg_port,
                    baudrate=19200,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE,
                    bytesize=serial.EIGHTBITS)

def calc(data):
    crc_table=[ 0x0000,0xC0C1,0xC181,0x0140,0xC301,0x03C0,0x0280,0xC241,
                0xC601,0x06C0,0x0780,0xC741,0x0500,0xC5C1,0xC481,0x0440,
                0xCC01,0x0CC0,0x0D80,0xCD41,0x0F00,0xCFC1,0xCE81,0x0E40,
                0x0A00,0xCAC1,0xCB81,0x0B40,0xC901,0x09C0,0x0880,0xC841,
                0xD801,0x18C0,0x1980,0xD941,0x1B00,0xDBC1,0xDA81,0x1A40,
                0x1E00,0xDEC1,0xDF81,0x1F40,0xDD01,0x1DC0,0x1C80,0xDC41,
                0x1400,0xD4C1,0xD581,0x1540,0xD701,0x17C0,0x1680,0xD641,
                0xD201,0x12C0,0x1380,0xD341,0x1100,0xD1C1,0xD081,0x1040,
                0xF001,0x30C0,0x3180,0xF141,0x3300,0xF3C1,0xF281,0x3240,
                0x3600,0xF6C1,0xF781,0x3740,0xF501,0x35C0,0x3480,0xF441,
                0x3C00,0xFCC1,0xFD81,0x3D40,0xFF01,0x3FC0,0x3E80,0xFE41,
                0xFA01,0x3AC0,0x3B80,0xFB41,0x3900,0xF9C1,0xF881,0x3840,
                0x2800,0xE8C1,0xE981,0x2940,0xEB01,0x2BC0,0x2A80,0xEA41,
                0xEE01,0x2EC0,0x2F80,0xEF41,0x2D00,0xEDC1,0xEC81,0x2C40,
                0xE401,0x24C0,0x2580,0xE541,0x2700,0xE7C1,0xE681,0x2640,
                0x2200,0xE2C1,0xE381,0x2340,0xE101,0x21C0,0x2080,0xE041,
                0xA001,0x60C0,0x6180,0xA141,0x6300,0xA3C1,0xA281,0x6240,
                0x6600,0xA6C1,0xA781,0x6740,0xA501,0x65C0,0x6480,0xA441,
                0x6C00,0xACC1,0xAD81,0x6D40,0xAF01,0x6FC0,0x6E80,0xAE41,
                0xAA01,0x6AC0,0x6B80,0xAB41,0x6900,0xA9C1,0xA881,0x6840,
                0x7800,0xB8C1,0xB981,0x7940,0xBB01,0x7BC0,0x7A80,0xBA41,
                0xBE01,0x7EC0,0x7F80,0xBF41,0x7D00,0xBDC1,0xBC81,0x7C40,
                0xB401,0x74C0,0x7580,0xB541,0x7700,0xB7C1,0xB681,0x7640,
                0x7200,0xB2C1,0xB381,0x7340,0xB101,0x71C0,0x7080,0xB041,
                0x5000,0x90C1,0x9181,0x5140,0x9301,0x53C0,0x5280,0x9241,
                0x9601,0x56C0,0x5780,0x9741,0x5500,0x95C1,0x9481,0x5440,
                0x9C01,0x5CC0,0x5D80,0x9D41,0x5F00,0x9FC1,0x9E81,0x5E40,
                0x5A00,0x9AC1,0x9B81,0x5B40,0x9901,0x59C0,0x5880,0x9841,
                0x8801,0x48C0,0x4980,0x8941,0x4B00,0x8BC1,0x8A81,0x4A40,
                0x4E00,0x8EC1,0x8F81,0x4F40,0x8D01,0x4DC0,0x4C80,0x8C41,
                0x4400,0x84C1,0x8581,0x4540,0x8701,0x47C0,0x4680,0x8641,
                0x8201,0x42C0,0x4380,0x8341,0x4100,0x81C1,0x8081,0x4040]

    crc_hi=0xFF
    crc_lo=0xFF

    for w in data:
        index = crc_lo ^ w
        crc_val = crc_table[index]
        crc_temp = int(crc_val / 256)
        crc_val_low = crc_val - (crc_temp * 256)
        crc_lo = crc_val_low ^ crc_hi
        crc_hi = crc_temp

    crc = crc_hi * 256 + crc_lo
    return crc

def print_green(line):
    print('\x1b\033[92m' + line + '\x1b[0m\n', flush=True)

def print_red(line):
    print('\x1b\033[91m' + line + '\x1b[0m\n', flush=True)

def process_cmd(command):
    if(ser.isOpen() == False):
        ser.open()

    cmd_bytes = bytearray.fromhex(command)

    crc = calc(cmd_bytes)
    crc_hi = crc >> 8
    crc_lo = crc & 0xFF
    cmd_bytes.append(crc_lo)
    cmd_bytes.append(crc_hi)
    print("send:       " + command + " %02X %02X" % (crc_lo, crc_hi), flush=True)

    ser.write(cmd_bytes)
    out = ''
    seq = []
    time.sleep(1)
    while ser.inWaiting() > 0:
        for c in ser.read():
            seq.append(c)
            out = ''.join(("%02X " % v) for v in seq)
            if chr(c) == '\n':
                break
    return str(out)

def test_modbus(command, is_valid=1):

    recieved = process_cmd(command)

    if len(recieved) > 2:
        print("  recieved: %s" % recieved, flush=True)

        is_err = int((recieved[3]), 16) & 0x8
        if(is_err):
            print("  is_error: %d" % is_err, flush=True)

        if is_valid > 1:
            print_red('\x1b\033[91m' + 'NOT EXPECTED RESULT!' + '\x1b[0m')
            return

        if ((not is_err and is_valid == 1) or (is_err and not (is_valid == 1))):
            print_green('\x1b\033[92m' + 'OK' + '\x1b[0m')
        else:
            print_red('\x1b\033[91m' + 'RESULT INCORRECT!' + '\x1b[0m')
    else:
        if(is_valid == 2):
            print_green('\x1b\033[92m' + 'NO RESULT CORRECT!' + '\x1b[0m')
        else:
            print_red('\x1b\033[91m' + 'NO RESULT RECIEVED' + '\x1b[0m')

EXPECT_ERROR    = 0  # ожидаем ответ c ошибкой
EXPECT_OK       = 1  # ожидаем корректный ответ
EXPECT_NORESP   = 2  # ожидаем отсутствие ответа

def modbus_start_test():
    test_modbus(echo_function,      EXPECT_OK)
    test_modbus(echo_function_2,    EXPECT_NORESP)
    test_modbus(get_poll_params,    EXPECT_OK)

    test_modbus(get_poll_params_2,  EXPECT_ERROR)
    test_modbus(get_poll_params_3,  EXPECT_NORESP)

    test_modbus(set_poll_params[0],    EXPECT_OK)
    test_modbus(set_poll_params[1],    EXPECT_ERROR)
    test_modbus(set_poll_params[2],    EXPECT_NORESP)

def test_cmd(cmd):
    result = process_cmd(cmd)
    if(result):
        print("recieved:   " + result, flush=True)
        print_green("OK")
    else:
        print_red("NO RESULT")

echo_function =   "14 08 00 00 00 00"
echo_function_2 = "14 08 00 00 00 00 00"  # invalid len

set_poll_params = [
    "14 10 FF FF 01 00 03 00 00 12", # частота выборки 1Гц, 16 каналов, 4 семпла
    "14 10 FF FF 11 11 05 00 00 11",    # wrong num bytes
    "14 10 FF FF 00 01 03 00 00 11 00", # wrong len
    "14 10 FF FF 02 00 03 00 00 12", # частота выборки 2Гц
    "14 10 FF FF 04 00 03 00 A0 12", # частота выборки 4Гц, 21 канал, 4 сэмпла, 0 стартовый канал
    "14 10 FF FF 01 00 03 00 F8 10", # старт канал больше 20 - должен перестать опрашивать каналы
    "14 10 FF FF 01 00 03 00 00 00", # частота выборки 1Гц, 0 каналов, 0 семпла
    "14 10 FF FF 02 00 03 00 21 04", # частота выборки 2Гц, 1 каналов, 1 семпла, начиная с 1
    "14 10 FF FF 00 00 03 00 21 04", # частота выборки 0Гц, 1 каналов, 1 семпла, начиная с 1
    "14 10 FF FF 01 00 03 00 C0 0C", # частота выборки 0Гц, 1 каналов, 1 семпла, начиная с 1
]

get_poll_params =   "14 03 FF FF 00 02"
get_poll_params_2 = "14 03 FF FF 00 01"     # invalid num bytes
get_poll_params_3 = "14 03 FF FF 00 02 00"  # invalid len

get_adc_values = [
    "14 04 FF FF 02 00",
    "14 04 FF FF 15 00",
    "14 04 FF FF 00 00",
    "14 04 FF FF 07 00",
    "14 04 FF FF 05 00",
]

# XXXXXX-XXXXX-XXXXX
#   Nz    Nk    No
# No - start channel
# Nk - active channels
# Nz - number samples

# ------------------------ START TESTS --------------------------
# modbus_start_test()
# test_cmd(echo_function)
# test_cmd(get_poll_params)


#   частота выборки 1Гц, 16 каналов, 4 семпла
# test_cmd(set_poll_params[0])
# test_cmd(get_poll_params)

#   опрос должен остановиться
# test_cmd(set_poll_params[5])
# test_cmd(get_poll_params)

# test_cmd(set_poll_params[8])
# test_cmd(get_poll_params)

#   опрос один канал
# test_cmd(set_poll_params[6])
# test_cmd(get_poll_params)

#   опрос два канала
# test_cmd(set_poll_params[7])
# test_cmd(get_poll_params)

#   опрос шесть каналов
# test_cmd(set_poll_params[9])
# test_cmd(get_poll_params)

# test_cmd(get_adc_values[1])
# test_cmd(get_adc_values[2])
# test_cmd(get_adc_values[3])
test_cmd(get_adc_values[4])

ser.close()
