#include "stm32f4xx.h"
#include <string.h>
#include <stdbool.h>
#include "stm32f4xx_hal_uart.h"
#include "ai_control.h"
#include "modbus.h"

#define     AI_ADC_MAX_NUM_CHANNELS 21  // количество измеряемых параметров, внешних переключаемых каналов
#define     AI_NUM_SAMPLES          16  // максимальный размер буфера для усреднения
#define     AI_ADC_BUFFER_SIZE      (AI_ADC_MAX_NUM_CHANNELS * AI_NUM_SAMPLES)

#define OUTPUT_PORT1        GPIOB
#define OUTPUT_PORT2        GPIOA
#ifdef UART_MOVED // ТОЛЬКО НА ВРЕМЯ РАЗРАБОТКИ !!!
    #define OUTPUT_PORT2_MASK   0x0633
#else
    #define OUTPUT_PORT2_MASK   0x003F
#endif

extern TIM_HandleTypeDef htim6;
extern TIM_HandleTypeDef htim7;
extern ADC_HandleTypeDef hadc1;

// не путать с каналами АЦП,
// каналы переключаются внешними реле, посадкой пинов на землю
// поэтому каналы опрашиваются по одному

struct
{
    bool        update;             // запуск новой цепочки измерений
    bool        eoc;                // end of conversion flag
    uint8_t     update_flag;        // с момента последнего запроса, или 0xFF если опрос не производится
                                        // обнулять при чтении
                                        // устанавливаeтся при старте окончании цикла опроса ацп
    uint16_t    poll_params;        // упакованные параметры опроса порта и датчиков
                                    // xxxxxx - xxxxx - xxxxx
                                    //  Nz       Nk      No
    uint16_t    poll_freq;          // частота опроса ADC в герцах, максимальное значение 0xFFFF,
                                    // но при такой частоте уже ADC не будет успевать производить оцифровку

    uint8_t     start_channel;      // с какого канала начинать опрос
    uint8_t     active_channels;    // количество опрашиваемых каналов - 1

    uint8_t     channel;            // текущий канал
    uint8_t     sample;             // текущий номер выборки

    uint8_t     num_samples;        // количество выборок по которым производится усреднение

    uint16_t    adc_value[AI_ADC_BUFFER_SIZE];
}ai_control;

extern UART_HandleTypeDef huart1;

static void set_poll_params(uint16_t poll_params)   // установка параметров опроса
{
    ai_control.start_channel = poll_params & 0x1F;    // интервал опроса порта входа, мс
    if(ai_control.start_channel > AI_ADC_MAX_NUM_CHANNELS)
    {
        ai_control.update_flag = 0xFF; // stop poll channels
    }
    ai_control.active_channels = (poll_params & 0x3E0) >> 5;
    if(ai_control.start_channel + ai_control.active_channels > AI_ADC_MAX_NUM_CHANNELS)
        ai_control.active_channels = AI_ADC_MAX_NUM_CHANNELS - ai_control.start_channel;

    ai_control.num_samples = (poll_params & 0xFC00) >> 10;
    if(ai_control.num_samples > AI_NUM_SAMPLES)
        ai_control.num_samples = AI_NUM_SAMPLES;
    ai_control.poll_params = poll_params;
}

static void set_poll_freq(uint16_t fs)
{
    HAL_TIM_Base_Stop_IT(&htim7);
    if(fs && (fs <= 0xFFFF))
    {
        ai_control.poll_freq = fs;
        htim7.Init.Period = 0xFFFF / ai_control.poll_freq;
        if (HAL_TIM_Base_Init(&htim7) != HAL_OK)
        {
            _Error_Handler(__FILE__, __LINE__);
        }
        HAL_TIM_Base_Start_IT(&htim7);
    }else{
        ai_control.update_flag = 0xFF;
        ai_control.poll_freq = 0;
    }
}

static void write_flash(uint8_t* data)
{
    HAL_FLASH_Unlock(); // unlock flash writing
    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_PGAERR | FLASH_FLAG_WRPERR);
    do{
        FLASH_Erase_Sector(DATA_SECTOR, FLASH_VOLTAGE_RANGE_3);
        HAL_StatusTypeDef status = HAL_OK;
        for (uint32_t i = 0; i < FLASH_DATA_SIZE; ++i)
        {
            status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, CONFIG_DATA_BEGIN + i, data[i]);
            if(status != HAL_OK)
                break;
        }

    }while(0);

    HAL_FLASH_Lock(); // lock the flash
}

static void read_flash(uint8_t* data, uint32_t size)
{
    uint32_t i;
    for(i = 0; i < size; ++i)
        data[i] = *(uint8_t*)(CONFIG_DATA_BEGIN + i);
}

// и тут и там происходит запись одинаковых данных, так как перезапись происходит секторами
static void save_data(void)
{
    uint8_t data[FLASH_DATA_SIZE];
    data[0] = ai_control.poll_params & 0xFF;
    data[1] = ai_control.poll_params >> 8;
    data[2] = ai_control.poll_freq & 0xFF;
    data[3] = ai_control.poll_freq >> 8;
    write_flash(data);
}

// установка параметров опроса с проверкой и сохранением во флеш
void ai_control_set_poll_params(uint16_t poll_params, uint16_t poll_freq)
{
    ai_control.update_flag = 0;
    bool do_save = false;
    if(poll_params != ai_control.poll_params)
    {
        set_poll_params(poll_params);
        do_save = true;
    }

    if(poll_freq != ai_control.poll_freq)
    {
        set_poll_freq(poll_freq);
        do_save = true;
    }

#ifdef OPTIONS_IN_FLASH
    if(do_save)
        save_data();
#endif
}

uint16_t ai_control_poll_freq(void) // установка параметров опроса
{
    return ai_control.poll_freq;
}

uint16_t ai_control_poll_params(void) // установка параметров опроса
{
    return ai_control.poll_params;
}

static void poll_params_init(uint16_t poll_params)
{
    if(!poll_params || (poll_params == 0xFFFF)) // при отсутствии сохраненных параметров
    {
        ai_control.poll_params = 0;
        ai_control.start_channel = 0;

        ai_control.poll_params |= (AI_NUM_SAMPLES << 5) & 0x3E0; // количество выборок для усреднения
        ai_control.num_samples = AI_NUM_SAMPLES;

        ai_control.poll_params |= (AI_ADC_MAX_NUM_CHANNELS << 10) & 0xFC00;
        ai_control.active_channels = AI_ADC_MAX_NUM_CHANNELS;
    }else{
        ai_control.poll_params = poll_params;
        set_poll_params(poll_params);
    }
}

uint8_t ai_control_adc_active_channels(void)
{
    return ai_control.active_channels;
}

uint8_t ai_control_update_flag(void)
{
    return ai_control.update_flag;
}

// сумма выборок для канала
uint16_t ai_control_adc_value_summ(uint8_t channel) // No + i
{
    if(ai_control.update_flag != 0xFF)
    {
        ai_control.update_flag = 0;

        uint16_t sum = 0;
        for(uint8_t i = 0; i < ai_control.num_samples + 1; ++i)
        {
            uint8_t index = (ai_control.start_channel + channel) * ((AI_NUM_SAMPLES + ai_control.sample - ai_control.num_samples - 1 + i) % AI_NUM_SAMPLES);
            sum += ai_control.adc_value[index];
        }
        return sum;
    }
    return 0;
}

// сумма квадратов выборок
uint32_t ai_control_adc_sqrt_value_summ(uint8_t channel)
{
    if(ai_control.update_flag != 0xFF)
    {
        ai_control.update_flag = 0;

        uint32_t sq_sum = 0;
        for(uint8_t i = 0; i < ai_control.num_samples + 1; ++i)
        {
            uint8_t index = (ai_control.start_channel + channel) * ((AI_NUM_SAMPLES + ai_control.sample - ai_control.num_samples - 1 + i) % AI_NUM_SAMPLES);
            sq_sum += ai_control.adc_value[index] * ai_control.adc_value[index];
        }
        return sq_sum;
    }
    return 0;
}

void ai_control_load_rom_data(void)
{
    uint8_t ai_control_params[FLASH_DATA_SIZE]; // packed poll params = 2
    memset(ai_control_params, 0, FLASH_DATA_SIZE);

    read_flash(ai_control_params, FLASH_DATA_SIZE);

    uint16_t poll_params = ai_control_params[0] | ai_control_params[1] << 8; // LSB first
    poll_params_init(poll_params);

    uint16_t poll_freq = ai_control_params[2] | ai_control_params[3] << 8;
    set_poll_freq(poll_freq);
}

void ai_control_update(void)
{
    ai_control.update = true; // запуск нового измерения по таймеру
}

// переключение реле сидящих на выходном порту
// в зависимости от текущего канала
void switch_relay(uint8_t channel)
{
    uint32_t output = 0xFFFFFFFF;
    switch(channel)
    {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
        case 15:
        case 16:
            // включает (открывает) 2 реле
            output = 3 << channel;
            OUTPUT_PORT1->ODR |= 0xFFFF;
            OUTPUT_PORT1->ODR &= ~output;
            OUTPUT_PORT2->ODR |= OUTPUT_PORT2_MASK;
            OUTPUT_PORT2->ODR &= ~(output >> 16);
            break;
#if UART_MOVED // ТОЛЬКО НА ВРЕМЯ РАЗРАБОТКИ !!! УДАЛИ ПРИ ПЕРЕНОСЕ НА F0
        case 17: // между A17-A0
            OUTPUT_PORT1->ODR = 0xFFFE; // вкл А0
            OUTPUT_PORT2->ODR |= OUTPUT_PORT2_MASK;
            // Enable divider for 300V
            OUTPUT_PORT2->ODR &= ~(0x2 & (1 << 5)); // вкл А17
            break;
        case 18: // между A18-A0
            OUTPUT_PORT1->ODR = 0xFFFE; // вкл А0
            OUTPUT_PORT2->ODR |= OUTPUT_PORT2_MASK;
            // Enable divider for 300V
            OUTPUT_PORT2->ODR &= ~((1 << 9) & (1 << 5)); // вкл А18 // ВМЕСТО РА2
            break;
        case 19: // между A19-0V
            OUTPUT_PORT1->ODR = 0xFFFF; //
            OUTPUT_PORT2->ODR |= OUTPUT_PORT2_MASK;
            // Enable divider for 300V
            OUTPUT_PORT2->ODR &= ~((1 << 10) & (1 << 5));
            break;
#else
        case 17: // между A17-A0
        case 18: // между A18-A0
            output = 1 << channel;
            OUTPUT_PORT1->ODR = 0xFFFE; // вкл А0
            OUTPUT_PORT2->ODR |= OUTPUT_PORT2_MASK;
            // Enable divider for 300V
            OUTPUT_PORT2->ODR &= ~((output >> 16) & (1 << 5));   // вкл А17 или А18
            break;
        case 19: // между A19-0V
#endif
        case 20: // между A20-0V
            output = 1 << channel;
            OUTPUT_PORT1->ODR = 0xFFFF;
            OUTPUT_PORT2->ODR |= OUTPUT_PORT2_MASK;
            // Enable divider for 300V
            OUTPUT_PORT2->ODR &= ~((output >> 16) & (1 << 5));
            break;
        default:
            break;
    }
}

void ai_control_eoc(void)
{
    ai_control.eoc = true;
    uint16_t index = (ai_control.channel + AI_ADC_MAX_NUM_CHANNELS * ai_control.sample) % AI_ADC_BUFFER_SIZE;
    ai_control.adc_value[index] = HAL_ADC_GetValue(&hadc1);
    ++ai_control.channel;
}

void on_timeout(void)
{
    HAL_TIM_Base_Stop_IT(&htim6);

    if(HAL_ADC_Start_IT(&hadc1) != HAL_OK)
        Error_Handler();
}

void start_adc_conversion(void)
{
    switch_relay(ai_control.channel);

    htim6.Init.Period = 1; // 1 ms
    if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
    {
        _Error_Handler(__FILE__, __LINE__);
    }
    HAL_TIM_Base_Start_IT(&htim6);
}

void ai_control_poll(void)
{
    if(ai_control.update_flag == 0xFF)
        return;

    // update запускает новое преобразование
    if(!ai_control.update)
    {
        return;
    }

    if(ai_control.eoc) // if last conversion ended
    {
        ai_control.eoc = false;
        if((ai_control.channel > ai_control.start_channel + ai_control.active_channels)
        || (ai_control.channel >= AI_ADC_MAX_NUM_CHANNELS)) // нумерация активного канала начинается с нуля, отсюда >=
        {
            ai_control.channel = ai_control.start_channel;
            ++ai_control.sample;
            ai_control.sample = ai_control.sample % AI_NUM_SAMPLES;
            ai_control.update_flag = 1;
        }
        start_adc_conversion();
        ai_control.update = false;
    }
}

void ai_control_init(void)
{
    ai_control.channel = 0;
    ai_control.sample = 0;
    ai_control.num_samples = AI_NUM_SAMPLES;
    ai_control.active_channels = AI_ADC_MAX_NUM_CHANNELS;
    ai_control.update_flag = 0;
    modbus_init();
    ai_control_load_rom_data();

    start_adc_conversion();
    ai_control.update = true;
}
