/*
    To enable circular buffer, you have to enable IDLE LINE DETECTION interrupt

    __HAL_UART_ENABLE_ITUART_HandleTypeDef *huart, UART_IT_IDLE);   // enable idle line interrupt
    __HAL_DMA_ENABLE_IT (DMA_HandleTypeDef *hdma, DMA_IT_TC);  // enable DMA Tx cplt interrupt
    also enable RECEIVE DMA
    HAL_UART_Receive_DMA (UART_HandleTypeDef *huart, DMA_RX_Buffer, 64);
    IF you want to transmit the received data uncomment lines

    PUT THE FOLLOWING IN THE MAIN.c

    #define DMA_RX_BUFFER_SIZE          64
    uint8_t DMA_RX_Buffer[DMA_RX_BUFFER_SIZE];

    #define UART_BUFFER_SIZE            256
    uint8_t UART_Buffer[UART_BUFFER_SIZE];
*/

#include "dma_usart_idle.h"
#include "string.h"
#include "modbus.h"

extern UART_HandleTypeDef huart1;
extern DMA_HandleTypeDef hdma_usart1_rx;

#define DMA_RX_BUFFER_SIZE          255
extern uint8_t DMA_RX_Buffer[DMA_RX_BUFFER_SIZE];

#define UART_BUFFER_SIZE            255
extern uint8_t UART_Buffer[UART_BUFFER_SIZE];

void USART_IrqHandler(UART_HandleTypeDef *huart, DMA_HandleTypeDef *hdma)
{
	if (huart->Instance->SR & UART_FLAG_IDLE)           /* if Idle flag is set */
	{
		volatile uint32_t tmp;                  /* Must be volatile to prevent optimizations */
        tmp = huart->Instance->SR;                       /* Read status register */
        tmp = huart->Instance->DR;                       /* Read data register */
        (void)tmp;
		hdma->Instance->CR &= ~DMA_SxCR_EN;       /* Disabling DMA will force transfer complete interrupt if enabled */
	}
}

void DMA_IrqHandler(DMA_HandleTypeDef *hdma)
{
	typedef struct
	{
		__IO uint32_t ISR;   /*!< DMA interrupt status register */
		__IO uint32_t Reserved0;
		__IO uint32_t IFCR;  /*!< DMA interrupt flag clear register */
	} DMA_Base_Registers;

	DMA_Base_Registers *regs = (DMA_Base_Registers *)hdma->StreamBaseAddress;

	if(__HAL_DMA_GET_IT_SOURCE(hdma, DMA_IT_TC) != RESET)   // if the source is TC
	{
	    /* Get the length of the data */
        size_t len = DMA_RX_BUFFER_SIZE - hdma->Instance->NDTR;
		/* Clear the transfer complete flag */
        regs->IFCR = DMA_FLAG_TCIF0_4 << hdma->StreamIndex;
        memcpy(&UART_Buffer[0], DMA_RX_Buffer, len);
        /* Prepare DMA for next transfer */
        /* Important! DMA stream won't start if all flags are not cleared first */
        regs->IFCR = 0x3FU << hdma->StreamIndex; // clear all interrupts
        hdma->Instance->M0AR = (uint32_t)DMA_RX_Buffer;   /* Set memory address for DMA again */
        hdma->Instance->NDTR = DMA_RX_BUFFER_SIZE;    /* Set number of bytes to receive */
        hdma->Instance->CR |= DMA_SxCR_EN;            /* Start DMA transfer */
        modbus_on_rtu(&UART_Buffer[0], len);
        memset(UART_Buffer, 0, len);
	}
}

void rs485_recieve_enable(void)
{
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);
}

void rs485_transmit_enable(void)
{
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);
}
