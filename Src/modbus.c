
// The MODBUS protocol defines three PDUs. They are :
// - MODBUS Request PDU,            mb_req_pdu
// - MODBUS Response PDU,           mb_rsp_pdu
// - MODBUS Exception Response PDU, mb_excep_rsp_pdu

// адрес устройства         1 byte
// номер функции            1 byte
// Адрес регистра           2 byte
// Количество флагов        2 byte
// Количество byte данных   1 byte
// Данные                   ...
// CRC                      2 byte

#include "modbus.h"
#include "stm32f4xx_hal_uart.h"
#include "ai_control.h"
#include "string.h"
#include "dma_usart_idle.h"

#define DEVICE_DEFAULT_ADDR     1
#define TXBUFFERSIZE            255

#define HAL_UART_TRANSMIT_DMA(BUF,LEN)      \
    rs485_transmit_enable();                \
    HAL_UART_Transmit_DMA(HUART, BUF, LEN);

extern UART_HandleTypeDef huart2;
#define HUART &huart2
struct{
    uint8_t     addr;
    uint8_t     dummy_counter;
    uint8_t     tx_buffer[TXBUFFERSIZE];
}modbus;

static unsigned char crc_hi[] =
{
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
    0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
    0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01,
    0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81,
    0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
    0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01,
    0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
    0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
    0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01,
    0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
    0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
    0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01,
    0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
    0x40
} ;

static char crc_lo[] =
{
    0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7, 0x05, 0xC5, 0xC4,
    0x04, 0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09,
    0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD,
    0x1D, 0x1C, 0xDC, 0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
    0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32, 0x36, 0xF6, 0xF7,
    0x37, 0xF5, 0x35, 0x34, 0xF4, 0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A,
    0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE,
    0x2E, 0x2F, 0xEF, 0x2D, 0xED, 0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
    0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 0x61, 0xA1, 0x63, 0xA3, 0xA2,
    0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F,
    0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68, 0x78, 0xB8, 0xB9, 0x79, 0xBB,
    0x7B, 0x7A, 0xBA, 0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
    0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0, 0x50, 0x90, 0x91,
    0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C,
    0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98, 0x88,
    0x48, 0x49, 0x89, 0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
    0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83, 0x41, 0x81, 0x80,
    0x40
};

uint16_t CRC16(unsigned char* msg, uint16_t data_len)
{
    unsigned char _crc_hi = 0xFF; /* high byte of CRC initialized */
    unsigned char _crc_lo = 0xFF; /* low byte of CRC initialized */
    unsigned index; /* will index into CRC lookup table */
    while (data_len--) /* pass through message buffer */
    {
        index = _crc_lo ^ *msg++; /* calculate the CRC */
        _crc_lo = _crc_hi ^ crc_hi[index];
        _crc_hi = crc_lo[index];
    }
    return (_crc_hi << 8 | _crc_lo);
}

void modbus_response_error(uint8_t func_num, uint8_t error_code)
{
    modbus.tx_buffer[0] = modbus.addr;
    modbus.tx_buffer[1] = func_num | 0x80;
    modbus.tx_buffer[2] = error_code;
    const uint16_t crc = CRC16(&modbus.tx_buffer[0], 3);
    modbus.tx_buffer[3] = crc & 0xFF;
    modbus.tx_buffer[4] = crc >> 8;
    HAL_UART_TRANSMIT_DMA(modbus.tx_buffer, 5);
}

void modbus_response_echo(uint8_t* message)
{
    volatile uint16_t _crc = message[7] << 8 | message[6];
    modbus.tx_buffer[0] = modbus.addr;
    modbus.tx_buffer[1] = 0x8;
    modbus.tx_buffer[2] = 0;
    modbus.tx_buffer[3] = 0;
    modbus.tx_buffer[4] = message[4];
    modbus.tx_buffer[5] = message[5];
    volatile const uint16_t calc_crc = CRC16(&modbus.tx_buffer[0], 6);
    if(calc_crc != _crc)
        return;
    modbus.tx_buffer[6] = calc_crc & 0xFF;
    modbus.tx_buffer[7] = calc_crc >> 8;
    HAL_UART_TRANSMIT_DMA(modbus.tx_buffer, 8);
}

static void get_poll_params(uint8_t* message)
{
    const uint16_t to_read = message[4] << 8 | message[5];
    if(to_read != 2)
    {
        modbus_response_error(message[1], 3);
        return;
    }
    volatile uint16_t _crc = message[7] << 8 | message[6];
    volatile uint16_t calc_crc = CRC16(&message[0], 6);
    if(calc_crc != _crc)
        return;

    modbus.tx_buffer[0] = modbus.addr;
    modbus.tx_buffer[1] = message[1];
    modbus.tx_buffer[2] = 4;
    uint16_t poll_freq = ai_control_poll_freq();
    modbus.tx_buffer[3] = poll_freq & 0xFF;
    modbus.tx_buffer[4] = poll_freq >> 8;
    uint16_t poll_params = ai_control_poll_params();
    modbus.tx_buffer[5] = poll_params & 0xFF;
    modbus.tx_buffer[6] = poll_params >> 8;
    const uint16_t crc = CRC16(&modbus.tx_buffer[0], 7);
    modbus.tx_buffer[7] = crc & 0xFF;
    modbus.tx_buffer[8] = crc >> 8;

    HAL_UART_TRANSMIT_DMA(modbus.tx_buffer, 9);
}

// запрос текущих значений параметров опроса
void modbus_f3(uint8_t* message, uint8_t size)
{
    const uint16_t reg_address = message[2] | message[3] << 8;

    if(reg_address == 0xFFFF) // get pop
    {
        get_poll_params(message);
    }else{
        modbus_response_error(message[1], 2);
    }
}

void get_ai_values(uint8_t* message)
{
    volatile uint16_t _crc = message[6] | message[7] << 8;
    volatile uint16_t calc_crc = CRC16(&message[0], 6);
    if(calc_crc != _crc)
        return;

    uint16_t channels_num = message[4] | message[5] << 8;
    uint8_t adc_active_channels = ai_control_adc_active_channels();

    if((channels_num < 2) || (channels_num > adc_active_channels + 2))
    {
        modbus_response_error(message[1], 3);
        return;
    }
    modbus.tx_buffer[0] = modbus.addr;
    modbus.tx_buffer[1] = message[1];
    modbus.tx_buffer[2] = (channels_num - 1) * 6 + 2;
    modbus.tx_buffer[3] = modbus.dummy_counter++;
    modbus.tx_buffer[4] = ai_control_update_flag();

    for(uint8_t i = 0; i < channels_num; ++i)
    {
        uint16_t val = ai_control_adc_value_summ(i); // сумма выборок
        modbus.tx_buffer[i * 2 + 5] = val & 0xFF;
        modbus.tx_buffer[i * 2 + 6] = val >> 8;
    }

    for(uint8_t i = 0; i < channels_num; ++i)
    {
        uint32_t sqrt_val = ai_control_adc_sqrt_value_summ(i); // сумма квадратов
        modbus.tx_buffer[i * 4 + 5 + channels_num * 2] = sqrt_val & 0xFF;
        modbus.tx_buffer[i * 4 + 6 + channels_num * 2] = (sqrt_val >> 8) & 0xFF;
        modbus.tx_buffer[i * 4 + 7 + channels_num * 2] = (sqrt_val >> 16) & 0xFF;
        modbus.tx_buffer[i * 4 + 8 + channels_num * 2] = sqrt_val >> 24;
    }

    const uint8_t data_len = channels_num * 6 + 5;

    const uint16_t crc = CRC16(&modbus.tx_buffer[0], data_len);
    modbus.tx_buffer[data_len] = crc & 0xFF;
    modbus.tx_buffer[data_len + 1] = crc >> 8;
    HAL_UART_TRANSMIT_DMA(modbus.tx_buffer, data_len + 2);
}

void modbus_f4(uint8_t* message, uint8_t size)
{
    const uint16_t reg_address = message[2] | message[3] << 8;
    if(reg_address == 0xFFFF)
    {
        get_ai_values(message);
    }else{
        modbus_response_error(message[1], 2);
        return;
    }
}

void modbus_f8(uint8_t* message, uint8_t size)
{
    if(message[2] | message[3])
        modbus_response_error(message[1], 1);
    else
        modbus_response_echo(message);
}

void set_poll_params_reg(uint8_t* message, uint8_t size)
{
    volatile uint16_t _crc = message[size - 1] << 8 | message[size - 2];
    volatile uint16_t calc_crc = CRC16(&message[0], size - 2);
    if(calc_crc != _crc)
        return;

    if(message[6] != 3)
    {
        modbus_response_error(message[1], 3);
        return;
    }
    if(size != 12)
        return;

    const uint16_t fs = message[4] | message[5] << 8; // частота опроса
    const uint16_t zr = message[8] | message[9] << 8; // параметры опроса
    ai_control_set_poll_params(zr, fs);

    modbus.tx_buffer[0] = modbus.addr;
    modbus.tx_buffer[1] = message[1];
    modbus.tx_buffer[2] = message[2];
    modbus.tx_buffer[3] = message[3];
    modbus.tx_buffer[4] = message[4];
    modbus.tx_buffer[5] = message[5];
    const uint16_t crc = CRC16(&modbus.tx_buffer[0], 6);
    modbus.tx_buffer[6] = crc & 0xFF;
    modbus.tx_buffer[7] = crc >> 8;
    // rs485_recieve_enable();
    HAL_UART_TRANSMIT_DMA(modbus.tx_buffer, 8);
}

void modbus_f16(uint8_t* message, uint8_t size)
{
    const uint16_t reg_address = message[2] | message[3] << 8;

    if(reg_address == 0xFFFF) // send io params register
    {
        set_poll_params_reg(message, size);
    }else{
        modbus_response_error(message[1], 2);
    }
}

void modbus_on_rtu(uint8_t* message, uint8_t size)
{
    const uint8_t addr = message[0];

    if(addr != modbus.addr)
        return;

    const uint8_t func_num = message[1];
    switch(func_num)
    {
        case 3:
            modbus_f3(message, size);
            break;
        case 4:
            modbus_f4(message, size);
            break;
        case 8:
            modbus_f8(message, size);
            break;
        case 16:
            modbus_f16(message, size);
            break;
        default:
            modbus_response_error(func_num, 1);
    }
}

void modbus_init(void)
{
    modbus.addr = 0x14;
    modbus.dummy_counter = 0;
}
