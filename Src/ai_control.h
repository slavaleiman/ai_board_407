#ifndef __AI_CONTROL_H__
#define __AI_CONTROL_H__

#include <stdbool.h>
#include "main.h"

// XXXXXX-XXXXX-XXXXX
//   Nz    Nk    No
// No - start channel
// Nk - active channels
// Nz - number samples

// commands
void        ai_control_poll(void);
void        ai_control_update(void);
void        ai_control_init(void);
void        ai_control_save_data(void);
void        ai_control_eoc(void);    // on end of conversion
void        on_timeout(void);
// get
uint16_t*   ai_control_adc_buffer(void);
uint16_t    ai_control_poll_params(void);           // параметры опроса
uint16_t    ai_control_poll_freq(void);             // частота опроса
uint8_t     ai_control_adc_active_channels(void);   // количество опрашиваемых каналов
uint8_t     ai_control_update_flag(void);
uint16_t    ai_control_adc_value_summ(uint8_t index);         // сумма выборок
uint32_t    ai_control_adc_sqrt_value_summ(uint8_t index);    // сумма квадратов выборок

// set
void ai_control_set_poll_params(uint16_t poll_params, uint16_t poll_freq); // установка параметров опроса

#endif /*__AI_CONTROL_H__*/
